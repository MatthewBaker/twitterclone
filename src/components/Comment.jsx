import React from 'react'

export default function Comment(props) {
    const { userName, content } = props.comment
    
    return (
        <div className="comment oneComment">
            <span className="avatar">
                <img src="https://images.pexels.com/photos/555790/pexels-photo-555790.png?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="avatar"/>
            </span>

            <div className="content">
                <span className="author">{userName}</span>

                <div className="metadata">
                    <span className="date">Today at 5:42PM</span>
                </div>

                <div className="text">
                    {content}
                </div>

                <div className="actions">
                    <span className="reply colorBlue">React</span>
                </div>
            </div>
        </div>
    )
}


