import React from 'react'

export default function Nav() {
    return (
      
        <div className="ui inverted pointing menu">
            <div className="ui container">
            <a href="/" className="active item">
                Feed
            </a>
            <a href="/" className="item">
                Messages
            </a>
            <a href="/" className="item">
                Friends
            </a>
            <div className="right menu">
                <div className="item">
                <div className="ui transparent icon input">
                    <input type="text" placeholder="Search..." />
                    <i className="search link icon colorWhite"></i>
                </div>
                </div>
            </div>
            </div>
        </div>    
     
    )
}
