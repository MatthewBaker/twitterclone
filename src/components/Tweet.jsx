import React, { Component } from 'react'
import SocialBar from './SocialBar'
import Comments from './Comments'
import AddComment from './AddComment'

class Tweet extends Component{
    state = {
        commentConfig: {
            isHidden: false
        }
    }
    
    toggleComment = () => {
        this.setState({
            commentConfig: {
                isHidden: !this.state.commentConfig.isHidden
            }
        })
    }

    onTweetClick = (e) =>{
        e.preventDefault()
        this.props.onTweetSelect(this.props.tweet.id)
    }

    textTruncate = function(str, length, ending) {
        if (length == null) {
          length = 100;
        }
        if (ending == null) {
          ending = '...';
        }
        if (str.length > length) {
          return str.substring(0, length - ending.length) + ending;
        } else {
          return str;
        }
    };

    render(){
        const { id, userName, content, image, title, likes, shares, comments } = this.props.tweet
        const { incrementLikes, incrementShares, addComment } = this.props
      
        return (
            <div className="ui card">
                <div className="image" onClick={this.onTweetClick}>
                    <div style={{ backgroundImage: `url(${image})`, height: '300px', backgroundPosition: 'center center', backgroundSize: 'cover'}} />
                </div>
    
                <div className="content" onClick={this.onTweetClick}>
                    <a href="https://google.com" className="header">{userName}</a>
                    <div className="meta">
                        <span className="date">{title}</span>
                    </div>
                    <div className="previewText">
                        {this.textTruncate(content, 85, '...')}
                    </div>
                </div>
                
                <div className="p1" style={{paddingBottom: '0.2em'}}>
                    <SocialBar
                        incrementLikes={incrementLikes}
                        incrementShares={incrementShares}
                        id={id}
                        likes={likes}
                        shares={shares}
                        comments={comments !== undefined ? comments.length : null}
                        toggleComment={this.toggleComment}
                    />
                 </div>

                {this.state.commentConfig.isHidden &&
                    <React.Fragment>
                        <AddComment id={id} addComment={addComment}/>

                        {comments.length > 0 &&
                            <Comments 
                                comments={comments}
                                id={id}
                            /> 
                        }
                    </React.Fragment>
                }
          </div>
        )
    }
}

export default Tweet