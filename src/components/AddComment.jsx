import React, { Component } from 'react'
import uuid from 'uuid'

export default class AddComment extends Component {
    constructor(props){
        super(props)

        this.state = {
            formContent: ''
        }
    }

    onCommentSubmit = (e) => {
        e.preventDefault()
    
        if(this.state.formContent !== ''){
            const comment = {
                id: new uuid(),
                userName: 'Jonh Doe',
                content: this.state.formContent
            }
    
            this.props.addComment(
                this.props.id,
                comment
            )

            this.setState({
                formContent: ''
            })
        }
    }

    onInputChange = (e) => {
        this.setState({
            formContent: e.target.value
        })
    }

    render() {

        return (
            <div className="addComment cardContainer">
                <form onSubmit={this.onCommentSubmit} className="ui reply form">
                    <div className="field">
                        <textarea 
                            autoFocus
                            rows="3"
                            value={this.state.formContent} 
                            type="text" 
                            onChange={this.onInputChange} 
                            placeholder='Message...'
                        />
                    </div>

                    <button className="ui basic button">React</button>
                </form>
            </div>
        )
    }
}


