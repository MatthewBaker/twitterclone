import React from 'react'
import Tweet from './Tweet'

export default function Tweets({tweets, incrementLikes, incrementShares, addComment, selectedTweetId, onTweetSelect}) {
    const tweetItems = tweets.map(tweet => {
        if(tweet.id !== selectedTweetId){
            return(
                <Tweet 
                    onTweetSelect={onTweetSelect}
                    addComment={addComment} 
                    incrementLikes={incrementLikes} 
                    incrementShares={incrementShares} 
                    tweet={tweet} 
                    key={tweet.id} 
                />
            )
        }   
        
        return null
    })

    return (
        <div className="ui container">
            {tweetItems}
        </div>
    )
}
