import React from 'react'

export default function SocialDropdownMenu(props) {
    console.log()
    return (
  

        <div className="ui dropdown">
        <div className="texts"><i className="ellipsis horizontal icon colorLightGrey" /></div>
        <i className="dropdown "></i>
        <div className="menu">
            <div className="item">
                <i className="colorGold star outline icon"></i> Add to Favourites
            </div>
            
            <div className="divider"></div>
            <div className="item">
            <i className="colorRed exclamation icon"></i> Report
            </div>
        </div>
        </div>
    )
}
