import React from 'react'
import Comment from './Comment'


export default function Comments(props) {
    const {comments} = props
    
    const commentItems = comments.map(comment => {
        return(
            <Comment 
                comment={comment} 
                key={comment.id} 
            />
        )
    })

    return (
        <div className="cardContainer">
            <div className="ui comments">
                {commentItems}
            </div>
        </div>
    )
}
