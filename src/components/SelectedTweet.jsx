import React, { Component } from "react";
import SocialBar from "./SocialBar";
import Comments from "./Comments";
import AddComment from "./AddComment";

class SelectedTweet extends Component {
  state = {
    commentConfig: {
      isHidden: true
    }
  };

  toggleComment = () => {
    this.setState({
      commentConfig: {
        isHidden: !this.state.commentConfig.isHidden
      }
    });
  };

  textTruncate = function(str, length, ending) {
    if (length == null) {
      length = 100;
    }
    if (ending == null) {
      ending = "...";
    }
    if (str.length > length) {
      return str.substring(0, length - ending.length) + ending;
    } else {
      return str;
    }
  };

  articleReadingTime = content => {
    return Math.round(content.length / 250) + " min read";
  };

  render() {
    const {
      id,
      userName,
      content,
      image,
      title,
      likes,
      shares,
      comments
    } = this.props.tweet;
    const { incrementLikes, incrementShares, addComment } = this.props;

    return (
      <article className="ui card">
        <div className="image">
          <div
            style={{
              backgroundImage: `url(${image})`,
              height: "500px",
              backgroundPosition: "center center",
              backgroundSize: "cover"
            }}
          />
        </div>

        <div className="content">
          <header>
            <span className="avatar">
              <img
                src="https://images.pexels.com/photos/555790/pexels-photo-555790.png?auto=compress&cs=tinysrgb&dpr=1&w=500"
                alt="avatar"
              />
            </span>
            <div className="meta">
              <div className="metaLeft">
                <a href="https://google.com" className="header">
                  {userName}
                </a>
                <h2 className="date">{title}</h2>
                <small className="mb_1">
                  {this.articleReadingTime(content)}
                </small>
              </div>

              <div className="metaRight mr_1">
                <time><small>9 hours ago</small></time>
              </div>
            </div>
          </header>

          <div className="description">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                venenatis maximus tellus mollis faucibus. Nunc felis neque, semper
                non ipsum a, consequat varius lacus. Vestibulum ornare diam metus,
                vel sagittis ligula gravida ac. Vivamus rutrum eros viverra velit
                rhoncus, in vulputate orci eleifend. Etiam elementum ex sed
                fermentum lacinia. Ut a risus id enim convallis lacinia. Aliquam
                erat volutpat.
            </p>
            
            <p>
                Proin sit amet felis ac sem imperdiet tempor ac sed
                elit. Vivamus elementum nec diam quis tincidunt. Phasellus ante
                mauris, mollis sed condimentum non, placerat congue ex. Curabitur
                mollis dui et tortor viverra, ut lacinia augue imperdiet. Aliquam
                porta porta sagittis. Ut semper congue sagittis. Cras non dui
                efficitur, sodales arcu nec, placerat justo. Nulla nec elit
                suscipit, vulputate augue in, dignissim quam. Donec id ante eu lorem
                dictum auctor.
            </p>
            
            <p>
                Nulla egestas sem ac enim scelerisque pulvinar.
                Aenean et dui vel augue euismod dapibus. Nullam sit amet libero
                tristique arcu placerat porta. Cras finibus mauris nec lorem luctus
                porttitor. Vestibulum ante ipsum primis in faucibus orci luctus et
                ultrices posuere cubilia Curae; Proin vel erat turpis. Proin non
                purus mi. Sed quis maximus ipsum. Nunc faucibus ultrices lacus.
                Vivamus augue nunc, accumsan ac tortor sit amet, hendrerit cursus
                neque. In sodales felis aliquet tortor ultrices, vel pharetra tortor
                elementum. Quisque lacus erat, placerat et eleifend id, iaculis in
                libero. Mauris elementum sodales viverra. Suspendisse erat mi,
                ornare ac viverra ut, euismod at risus. Vivamus et risus egestas,
                aliquam risus sit amet, bibendum tortor. Quisque eu massa ante.
                Vestibulum pellentesque ultricies iaculis. Praesent rutrum non urna
                sit amet imperdiet.
            </p>
            </div>

        </div>

        <div className="p1">
          <SocialBar
            showOptions={true}
            incrementLikes={incrementLikes}
            incrementShares={incrementShares}
            id={id}
            likes={likes}
            shares={shares}
            comments={comments !== undefined ? comments.length : null}
            toggleComment={this.toggleComment}
          />
        </div>
        {this.state.commentConfig.isHidden && (
          <React.Fragment>

            <AddComment id={id} addComment={addComment} />

            {comments.length > 0 && comments !== undefined && (
              <Comments comments={comments} id={id} />
            )}
          </React.Fragment>
        )}
      </article>
    );
  }
}

export default SelectedTweet;
