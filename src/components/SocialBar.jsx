import React from "react";
import SocialDropdownMenu from './SocialDropdownMenu'

class SocialBar extends React.Component {
  onLikeHandle = e => {
    e.preventDefault();
    this.props.incrementLikes(this.props.id);
  };

  onShareHandle = e => {
    e.preventDefault();
    this.props.incrementShares(this.props.id);
  };

  onCommentToggle = e => {
    e.preventDefault();
    this.props.toggleComment(this.props.id);
  };

  onAddFavourites = e => {
      e.preventDefault()

      alert(1)
  }

  render() {
    const { likes, shares, comments, showOptions } = this.props;
    const likesClassConfig = ["heart", "icon", likes === 0 && "outline"];

    console.log(showOptions)
    const commentClassConfig = [
      "colorBlue",
      "comment",
      "outline",
      "icon",
      comments !== 0 && "alternate"
    ];

    return (
      <div className="socialBar extra content">
        <span className="socialHandle" onClick={this.onLikeHandle}>
          <i className={likesClassConfig.join(" ")} />
          <span className="likeCount">{likes}</span>
        </span>

        <span className="socialHandle" onClick={this.onCommentToggle}>
          <i className={commentClassConfig.join(" ")} />
          <span className="colorBlue commentCount">{comments}</span>
        </span>

        <span className="socialHandle grow" onClick={this.onShareHandle}>
          <i className="colorOrange share icon" />
          <span className="colorOrange shareCount">{shares}</span>
        </span>

        {showOptions === true && 
            <SocialDropdownMenu onAddFavourites={this.onAddFavourites}/>
        }
       
      </div>
    );
  }
}

export default SocialBar;
