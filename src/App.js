import React, { Component } from 'react'
import Nav from './components/Nav'
import Tweets from './components/Tweets'
import SelectedTweet from './components/SelectedTweet'
import FilterMenu from './components/FilterMenu'

import './App.css'

class App extends Component {
  constructor(props){
    super(props)

    this.state = {
      isMounted: false,
      tweets: [
        {
          id: 1,
          userName: 'Julian',
          title: 'Learning ReactJS',
          author: 'Matthew Schiebe',
          image: 'https://images.unsplash.com/photo-1464881522406-d5f66adb3e3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1955&q=80',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis tellus et ex aliquam aliquam. Mauris dapibus leo sed ante dignissim bibendum. Mauris gravida dui vehicula varius eleifend. Duis quis feugiat elit. Etiam sed aliquam risus. Praesent fringilla sapien eget blandit eleifend. Duis interdum commodo enim vitae aliquam. Ut molestie massa ut nisl consequat, pulvinar aliquet nunc vulputate. Maecenas eget diam ultrices, egestas ante ut, aliquet felis. Proin lectus felis, euismod ac nibh in, molestie suscipit purus. Quisque ac diam porta, tempor augue vitae, tempor sem. Phasellus commodo orci nec leo dictum, ac posuere dui varius. Quisque ut dolor malesuada, viverra dui ut, hendrerit eros. ',
          likes: 2,
          shares: 0,
          comments: [
            {
              id: 1,
              userName: 'Julian',
              content: 'bla bla bla'
            },
            {
              id: 2,
              userName: 'Julian',
              content: 'bla bla bla'
            },
            {
              id: 3,
              userName: 'Julian',
              content: 'bla bla bla'
            },
          ]
        },
        {
          id: 2,
          userName: 'Matthew',
          title: 'Learning ReactJS',
          author: 'Julian Baker',
          image: 'https://images.unsplash.com/photo-1468434453985-b1ca3b555f00?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis tellus et ex aliquam aliquam. Mauris dapibus leo sed ante dignissim bibendum. Mauris gravida dui vehicula varius eleifend. Duis quis feugiat elit. Etiam sed aliquam risus. Praesent fringilla sapien eget blandit eleifend. Duis interdum commodo enim vitae aliquam. Ut molestie massa ut nisl consequat, pulvinar aliquet nunc vulputate. Maecenas eget diam ultrices, egestas ante ut, aliquet felis. Proin lectus felis, euismod ac nibh in, molestie suscipit purus. Quisque ac diam porta, tempor augue vitae, tempor sem. Phasellus commodo orci nec leo dictum, ac posuere dui varius. Quisque ut dolor malesuada, viverra dui ut, hendrerit eros. ',
          likes: 0,
          shares: 0,
          comments: []
        },
        {
          id: 3,
          userName: 'Stefin',
          title: 'Learning ReactJS',
          author: 'Matthew Stefiebe',
          image: 'https://images.pexels.com/photos/33688/delicate-arch-night-stars-landscape.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          content: 'This is some random really dummy text',
          likes: 0,
          shares: 0,
          comments: []
        },
        {
          id: 4,
          userName: 'Petra',
          title: 'Learning ReactJS',
          author: 'Julian Stefiebe',
          image: 'https://images.unsplash.com/photo-1554435445-df90e000bf99?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
          content: 'This is some random really dumb dummy text',
          likes: 0,
          shares: 0,
          comments: []
        },
        {
          id: 5,
          userName: 'Petra',
          title: 'Learning ReactJS',
          author: 'Julian Stefiebe',
          image: 'https://images.unsplash.com/photo-1554435445-df90e000bf99?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
          content: 'This is some random really dumb dummy text',
          likes: 0,
          shares: 0,
          comments: []
        },
          {
          id: 6,
          userName: 'Petra',
          title: 'Learning ReactJS',
          author: 'Julian Stefiebe',
          image: 'https://images.unsplash.com/photo-1554435445-df90e000bf99?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
          content: 'This is some random really dumb dummy text',
          likes: 0,
          shares: 0,
          comments: []
        }
      ],
      selectedTweet: {}
    }
  }

  componentDidMount(){
    const selectedTweet = this.state.tweets[0]
  
    this.setState({
      selectedTweet,
      isMounted: true
    })
  }

  addComment = (tweetId, comment) => {
    const tweetsClone = [...this.state.tweets]

    tweetsClone.map(tweet => {
      tweet.id === tweetId && console.log(tweet)    
      return tweet.id === tweetId && tweet.comments.push(comment) 
    })

    this.setState({tweetsClone})
  }

  incrementLikes = (id) => {
    const tweetsClone = [...this.state.tweets]
    
    tweetsClone.map(tweet => {
      return tweet.id === id && tweet.likes ++ 
    })

    this.setState({tweetsClone})
  }

  incrementShares = (id) => {
    const sharesClone = [...this.state.tweets]
    
    sharesClone.map(tweet => {
       return tweet.id === id && tweet.shares ++ 
    })

    this.setState({sharesClone})
  }
  

  onTweetSelect = (id) =>{
    //map through state tweets
    this.state.tweets.map((tweet) => {
      //if tweet ids match

      //update state with current tweet
      if(tweet.id === id){
        this.setState({
          selectedTweet: tweet
        })
      }

      return tweet
     
    })
  }

  render(){
     
    return (
      <div className="appContainer">
        <Nav />
        <div className="ui container">

          <div className='ui grid'>

  
            <div class="ui small breadcrumb">
              <a class="section">Home</a>
              <i class="right chevron icon divider"></i>
              <a class="section">Registration</a>
              <i class="right chevron icon divider"></i>
              <div class="active section">Personal Information</div>
            </div>
            <br />
          </div>
          <div className='ui grid'>
              
              <div className='mainLeft eleven wide column'>
                {this.state.isMounted &&
                  <SelectedTweet 
                    addComment={this.addComment} 
                    incrementLikes={this.incrementLikes} 
                    incrementShares={this.incrementShares} 
                    tweet={this.state.selectedTweet}
                  />  
                }
              </div>

              <div className='mainRight five wide column'>
                  <FilterMenu /> <br></br>
                  <Tweets 
                    onTweetSelect={this.onTweetSelect}
                    selectedTweetId={this.state.selectedTweet.id}
                    addComment={this.addComment} 
                    incrementLikes={this.incrementLikes} 
                    incrementShares={this.incrementShares} 
                    tweets={this.state.tweets}
                  />
                </div>
              </div>
        </div>
      </div> 
    )
  }
}

export default App
